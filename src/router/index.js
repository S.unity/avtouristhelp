import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/wins',
    name: 'Wins',
    component: () => import('../components/Wins')
  },
  {
    path: '/resheniya',
    name: 'Ebsa',
    component: () => import('../components/Ebsa')
  },
  {
    path: '/chance',
    name: 'chance',
    component: () => import('../components/chance')
  },
  {
    path: '/thanks',
    name: 'Thanks',
    component: () => import('../components/Thanks')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
